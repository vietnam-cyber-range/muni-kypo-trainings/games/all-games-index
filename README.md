# The listing of all cybersecurity games and common instructions

* [Junior hacker training](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/junior-hacker)
* [Secret laboratory](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/secret-laboratory)
* [Locust 3302](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/locust-3302)
* [Community-hosted games](external-links.md)

## General information

All the listed games work inside two interactive learning environments: **KYPO Cyber Range Platform** and **Cyber Sandbox Creator**.
* To deploy a selected game in the [KYPO Cyber Range Platform](https://crp.kypo.muni.cz), please follow [these steps](https://docs.crp.kypo.muni.cz/basic-concepts/typical-training-workflow/training-workflow-cloud/).
* To deploy a selected game locally using the [Cyber Sandbox Creator](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator), continue reading this guide.

## Requirements

* A standard personal computer or laptop with Windows, Linux, or Mac OS X operating system.
* At least 8 GB of free RAM and 4 vCPU (virtual CPU) cores.
* At least 20 GB of free disk space.
* Internet connection.
* Install the prerequisites for running the sandboxes generated by [Cyber Sandbox Creator](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator/-/wikis/3.0/Installation).

## Instructions

1. **Clone the game's repository** locally using `git clone https://gitlab.ics.muni.cz/muni-kypo-trainings/games/<repository-name>.git`.

2. **Open a command-line interface** in the game repository's root folder.

3. Run the command `manage-sandbox build --ansible-vars student_id:12345`, but replace `12345` with your personal "student ID" (a **unique alphanumerical string** at your institution). This will **instantiate the sandbox** for the game. 
	* This stage can take some time, please be patient :) The first setup might take from 10 minutes up to an hour (depending on your Internet connection speed) to download the operating system boxes for Vagrant. Each subsequent setup should take from 10 to 20 minutes. In the meantime, feel free to work at something else; the process does not require any input from you.
	* If you experience any technical issues at this stage, check out the [Cyber Sandbox Creator wiki](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator/-/wikis/3.0/Known-Issues) for troubleshooting tips.
<br/>

4. **Log into the attacker machine**: either via VirtualBox GUI using the login `kali` and password `kali`, or via the command `vagrant ssh attacker` and escalate your privileges to `root` with `sudo su`.

5. If the game repository contains the `game_design.md` or `game_design.pdf` files, **follow the assignments** and the storyline in these files. (They have identical content, so choose whichever format you prefer.) If these files are *not* present, then either your institution will deploy a CTFd instance with the assignments, or contact us and ask for the files (see the *Contact* section below).

## Information about data collection for research

We would like to ask for your consent to use **anonymized data** from solving the game tasks for **academic research**. After you create and access the game sandbox, we may collect the **shell commands** that you execute in the sandbox, as well the following metadata: timestamp of the command execution, working directory, username within the Vagrant box, IP address of the machine in the sandbox, and IP address of the hosting computer. After you finish playing, your personal data will be irreversibly anonymized and further used only for scientific and research purposes. By building the sandbox from this repository, you agree to these conditions.

If you require more information, you can:
* Read about our research on the website of [Cybersecurity Laboratory](https://kypo.fi.muni.cz).
* Investigate the contents of this open-source repository to check the data collection implementation for yourself.
* Contact us if you have any further questions (see the *Contact* section below).

If for any reason you still feel uncomfortable with this, each game has a branch that does not contain any logging modules. However, it would help our work tremendously if you chose the logging version. Thank you :)

## License

All games are licensed under the [MIT license](https://opensource.org/licenses/MIT) for the code (Vagrant, Ansible, and optionally custom code) and [CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0) for the game design.

## How to cite the games

If you use any of the games in any published work, please link to the game's repository and also cite the following paper:

Valdemar Švábenský, Jan Vykopal, Milan Cermak, and Martin Laštovička.\
*Enhancing cybersecurity skills by creating serious games.*\
In Proceedings of the 23rd Annual ACM Conference on Innovation and Technology in Computer Science Education (ITiCSE 2018).\
DOI: https://doi.org/10.1145/3197091.3197123

```
@inproceedings{Svabensky2018enhancing,
    author    = {\v{S}v\'{a}bensk\'{y}, Valdemar and Vykopal, Jan and Cermak, Milan and La\v{s}tovi\v{c}ka, Martin},
    title     = {{Enhancing Cybersecurity Skills by Creating Serious Games}},
    booktitle = {Proceedings of the 23rd Annual ACM Conference on Innovation and Technology in Computer Science Education},
    series    = {ITiCSE 2018},
    year      = {2018},
    isbn      = {978-1-4503-5707-4},
    location  = {Larnaca, Cyprus},
    pages     = {194--199},
    numpages  = {6},
    url       = {http://doi.acm.org/10.1145/3197091.3197123},
    doi       = {10.1145/3197091.3197123},
    acmid     = {3197123},
    publisher = {ACM},
    address   = {New York, NY, USA},
}
```

## Contact

[Cybersecurity Laboratory](https://cybersec.fi.muni.cz)\
Faculty of Informatics\
Masaryk University

Feel free to contact [Valdemar Švábenský](mailto:svabensky@ics.muni.cz?subject=MUNI%20KYPO%20Cybersecurity%20Games) if you have any feedback, requests, or questions.\
Also, the hints, solutions, and flag values for the games are available on request.
